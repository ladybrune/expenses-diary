$(document).one('pageinit', function(){ //inicijalizacija stranice samo jednom
	var zbir=0;

	//prikazivanje troskova
	prikaziTroskove();

	// add handler
	$("#potvrdiUnos").on('tap', dodajTrosak);

	// edit handler
	$("#potvrdiIzmenjeniUnos").on('tap', izmeniTrosak);

	// delete handler
	$("#beleske").on('tap', '#obrisiLink', obrisiTrosak);

	// set current handler
	$("#beleske").on('tap','#izmenjeniLink', setTrenutni);

	// obrisi sve handler
	$("#obrisiTroskove").on('tap', obrisiSve);

	/*
		* prikazi sve troskove na pocetnoj stranici
	*/
	function prikaziTroskove(){
		//get objects troskovi
		var troskovi=getTroskoviObject();	
		
		$("#zbir").html('Zbir svih troškova je &nbsp;'+ zbir+ '&nbsp;dinara');
		//}

		//check if empty
		if(troskovi !='' && troskovi!=null){
			for (var i = 0; i<troskovi.length; i++) {
				$("#beleske").append('<li class="ui-body-inherit ui-li-static"><strong>Ime:</strong>'+troskovi[i]["ime"]+
					'<br><strong>Iznos: </strong>'+troskovi[i]["iznos"] +' RSD<br><strong>Datum: </strong>'+troskovi[i]["datum"]+'<div class="controls">'+
					'<a href="#izmeni" id="izmenjeniLink" data-ime="'+troskovi[i]["ime"]+'" data-iznos="'+troskovi[i]["iznos"]+'" data-datum="'+troskovi[i]["datum"]+'">Izmeni</a> | <a href="#" id="obrisiLink" data-ime="'+troskovi[i]["ime"]+'" data-iznos="'+troskovi[i]["iznos"]+'" data-datum="'+troskovi[i]["datum"]+'" onclick="return confirm (\'Potvrdite brisanje troška\')">Obriši</a></li>');
			}
			$("#pocetna").bind('pageinit', function(){
					$("#beleske").listview('refresh');
			});	
			
		}else{
			$("#beleske").html('<p>Nemate unete troškove!</p>');
			
		}
		
	}

	/*
		* Dodaj trosak
		data-iznos="'+troskovi[i]["iznos"]+'" data-datum="'+troskovi[i]["datum"]+'" data-ime="'+troskovi[i]["ime"]+'"
		'<div class="controls">'
	*/

	function dodajTrosak(){
		//value have to be string in localStorage
		// get form values and put them into variables
		var iznos=$('#iznosTroska').val();
		var datum=$('#datumTroska').val();
		var ime=$('#imeTroska').val();

		//Create 'trosak' objekat
		var trosak={
			ime: ime,
			iznos: parseFloat(iznos),
			datum: datum		
			
		};

		var troskovi= getTroskoviObject();
		

		//dodaj trosak nizu troskova 'push metoda'
		troskovi.push(trosak);
		alert('Trošak je dodat');

		//stringify jer ono sto dadajem u localStorage mora biti STRING!!!
		//JSON ispred jer se radi o objektu!!!
		localStorage.setItem('troskovi',JSON.stringify(troskovi));

		//redirect to index page
		window.location.href="index.html";
		return false;

	}

	/*
		* izmeni trosak
	*/
	function izmeniTrosak(){

		//get current data
		trenutnoIme=localStorage.getItem('trenutnoIme');
		trenutniIznos=localStorage.getItem('trenutniIznos');
		trenutniDatum=localStorage.getItem('trenutniDatum');
		
		//value have to be string in localStorage
		var troskovi= getTroskoviObject();
		
		//loop through troskovi
		for(var i=0; i<troskovi.length; i++){
			if(troskovi[i].ime==trenutnoIme && troskovi[i].iznos== trenutniIznos && troskovi[i].datum==trenutniDatum  ){
				troskovi.splice(i, 1);
			}
			localStorage.setItem('troskovi', JSON.stringify(troskovi));
		}		

		// get form values and put them into variables
		var ime=$("#izmenjenoImeTroska").val();
		var iznos=$("#izmenjeniIznosTroska").val();
		var datum=$("#izmenjeniDatumTroska").val();
		

		//Create 'azurirantrosak' objekat
		var azuriranTrosak={
			ime: ime,
			iznos: parseFloat(iznos),
			datum: datum		
			
		};

		//dodaj trosak nizu troskova 'push metoda'
		troskovi.push(azuriranTrosak);
		alert('Trošak je ažuriran');

		//stringify jer ono sto dadajem u localStorage mora biti STRING!!!
		//JSON ispred jer se radi o objektu!!!
		localStorage.setItem('troskovi',JSON.stringify(troskovi));

		//redirect to index page
		window.location.href="index.html";
		return false;
	}

	/*
		* obrisi trosak
	*/
	function obrisiTrosak(){
		//set ls items
		localStorage.setItem('trenutnoIme', $(this).data('ime'));
		localStorage.setItem('trenutniIznos', $(this).data('iznos'));
		localStorage.setItem('trenutniDatum', $(this).data('datum'));
		//get current data
		trenutnoIme=localStorage.getItem('trenutnoIme');
		trenutniIznos=localStorage.getItem('trenutniIznos');
		trenutniDatum=localStorage.getItem('trenutniDatum');
		
		//value have to be string in localStorage
		var troskovi= getTroskoviObject();
		
		//loop through troskovi
		for(var i=0; i<troskovi.length; i++){
			if(troskovi[i].ime==trenutnoIme && troskovi[i].iznos== trenutniIznos && troskovi[i].datum==trenutniDatum  ){
				troskovi.splice(i, 1);
			}
			localStorage.setItem('troskovi', JSON.stringify(troskovi));
		}
		
		alert('Trošak je obrisan');

		//redirect to index page
		window.location.href="index.html";
		return false;
	}


	/*
		* obrisi sve
	*/
	function obrisiSve(){
		alert('Potvrdite');
		localStorage.removeItem('troskovi');
		$("#beleske").html('<p>Nemate unete troškove!</p>');
		zbir=0;
		//$("#zbir").css("display", "none");
		$("#pocetna").bind('pageinit', function(){
					$("#beleske").listview('refresh');
			});

			window.location.href="index.html";

	}	

	
	/*
		* Uzmi trosak objekat
	*/
	function getTroskoviObject(){
		//  set niz troskova kreiramo prazan
		var troskovi= new Array();	
		var   tekuciTroskovi= localStorage.getItem('troskovi');
		//provera da li ima neki element u localStorage
		if(tekuciTroskovi != null){
			//set to troskovi
			var troskovi=JSON.parse(tekuciTroskovi);
			zbir = 0;

			$.each(troskovi, function () {
    			zbir += JSON.parse(this.iznos);

				});
			$("#zbir").css("display", "inline");					

		}
		//return troskovi objekat
		return troskovi.sort(function(a, b){return new Date (b.date) - new Date(a.date)});


	}	

	/*
		* set current clicked iznos i datum
	*/
	function setTrenutni(){
		
		//set ls items
		localStorage.setItem('trenutnoIme', $(this).data('ime'));
		localStorage.setItem('trenutniIznos', $(this).data('iznos'));
		localStorage.setItem('trenutniDatum', $(this).data('datum'));		

		//insert this in edit form
		$("#izmenjeniIznosTroska").val(localStorage.getItem('trenutniIznos'));
		$("#izmenjeniDatumTroska").val(localStorage.getItem('trenutniDatum'));
		$("#izmenjenoImeTroska").val(localStorage.getItem('trenutnoIme'));

	}
});
